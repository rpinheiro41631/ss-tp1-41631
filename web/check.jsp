<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.CrypService" %>
<%@ page import="servlet.Account" %>
<%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 17-11-2017
  Time: 6:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Authorized!</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<div class="container" style="margin-top:30px; font-family: 'Muli', sans-serif;">
    <div class="jumbotron">
        <p id="welldonetext">Well done!</p>
        <br>
        <%
            String username = request.getParameter("username");
            String pwd = request.getParameter("pwd");
            CrypService cp = new CrypService();
            String pwdEnc = cp.doEncryption(pwd);
            Authenticator auth = new Authenticator();
            Account acc = null;
            try {
               acc = auth.login(username, pwdEnc);
            } catch(Exception e) {
                e.printStackTrace();
            }
            if(acc == null) {
                response.sendRedirect("/error.jsp");
            } else {
                HttpSession httpS = request.getSession(true);
                httpS.setAttribute("USER", acc.getUsername());
                httpS.setAttribute("PWD", acc.getPassword());
                response.sendRedirect("/home.jsp");
            }
        %>
    </div>
</div>
</body>
</html>
