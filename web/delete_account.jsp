<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.Account" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 22:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete Account</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
    <div class="jumbotron">
        <div class="container" style="margin-top:30px; font-family: 'Muli', sans-serif;">
            <%
                HttpSession hs = request.getSession(true);
                String un_aux = hs.getAttribute("USER").toString();
                Authenticator as = new Authenticator();
                Account acc = as.getAccount(un_aux);
                if(acc.getAdminStatus()) {
                    out.println("<form name=\"deleteAcc\" action=\"/intermediate.jsp\" method=\"POST\">");
                    out.println("<div class=\"form-group\">");
                    out.println("<label for=\"username\">User to delete:</label>");
                    out.println("<input type=\"text\" id=\"username\" name=\"username\">");
                    out.println("</div>");
                    out.println("<button type=\"submit\" class=\"btn btn-default\">Submit</button>");
                    out.println("<input type=\"hidden\" value=redirect_url>");
                    out.println("</form>");
                    //
                }
            %>
        </div>
    </div>
</body>
</html>
