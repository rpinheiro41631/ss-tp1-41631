<%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 3:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<div class="container" style="margin-top:30px; font-family: 'Muli', sans-serif;">
    <div class="jumbotron">
        <p style="font-family: 'Muli', sans-serif;">Error.</p><br>
        <a href="/index.jsp" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-home"></span> Home
        </button>
    </div>
</div>
</body>
</html>
