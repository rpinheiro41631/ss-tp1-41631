<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.Account" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 2:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
    <%
        /*servlet.Authenticator as = new servlet.Authenticator();
        servlet.Account acc;
        HttpSession hs = request.getSession(true);
        String aux = hs.getAttribute("USER").toString();
        acc = as.getAccount(aux);
        boolean state = acc.getLoginStatus();
        if(!state) {
            response.sendRedirect("/index.jsp");
        }*/


    %>

    <div class="container"  style="margin-top:30px; font-family: 'Muli', sans-serif;">
        <div class="jumbotron">
            <a href="/logout.jsp" class="btn btn-default">Logout</a>
            <a href="/change_pass.jsp" class="btn btn-default">Change password</a>
            <%
                HttpSession hs = request.getSession(true);
                String aux = hs.getAttribute("USER").toString();
                Authenticator as = new Authenticator();
                Account acc = as.getAccount(aux);
                if(acc.getAdminStatus()) {
                    out.println("<a href=\"/factory\" class=\"btn btn-primary\">Create an Account</a>");
                    out.println("<a href=\"/delete_account.jsp\" class=\"btn btn-primary\">Delete Account</a>");
                }
            %>
        </div>
    </div>

    <%
    // if user.islogged()
    %>
</body>
</html>
