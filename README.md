**Instructions for installment**
**1** Download mySQL for your computer and connect to the software (to create the first account, use this link: https://www.linode.com/docs/databases/mysql/install-mysql-on-ubuntu-14-04).
`mysql -u root -p`

**2** Create a database with a table with the following script:

`CREATE TABLE sal_database;`


`CREATE TABLE users (
   id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(200) NOT NULL,
   password VARCHAR(200) NOT NULL,
   isLogged BOOLEAN NOT NULL,
   isLocked BOOLEAN NOT NULL,
   isAdmin BOOLEAN NOT NULL
  );`
  
**3** Download the JBDC driver from here: https://dev.mysql.com/downloads/connector/j/

**4** Configure Tomcat (right version: 8.5.3 -- the 9.x version is still in beta: [DOWNLOAD] https://tomcat.apache.org/download-80.cgi#8.5.23).<br>
For InteliJ: https://www.jetbrains.com/help/idea/run-debug-configuration-tomcat-server.html
For Eclipse: http://www.coreservlets.com/Apache-Tomcat-Tutorial/tomcat-7-with-eclipse.html

**5** Run the server. The link should be in localhost:8081. Test with some operations.

**Alternative**: simply run the Maven command relative to build the project.
Something like this: 
`mvn package`
