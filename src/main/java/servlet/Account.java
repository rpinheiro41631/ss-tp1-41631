package servlet;

public class Account {
    public String username;
    public String password;
    public boolean isLogged;
    public boolean isLocked;
    public boolean isAdmin;

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
        isLogged = false;
        isLocked = false;
        isAdmin = false;
    }

    public Account(String username, String password, boolean isLogged, boolean isLocked, boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.isLogged = isLogged;
        this.isLocked = isLocked;
        this.isAdmin = isAdmin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean getLoginStatus() {
        return isLogged;
    }

    public boolean getLockStatus() {
        return isLocked;
    }

    public boolean getAdminStatus() {
        return isAdmin;
    }

    public void changeLockStatus() {
        isLocked = true;
    }

    public void changeLoginStatus() {
        isLogged = true;
    }

    public void changePassword(String newPassword) {
        this.password = newPassword;
    }
}
