package servlet;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

@WebServlet(name = "servlet.Authenticator")
public class Authenticator extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String HTML_TAG_1 = "<html>";
    private static final String HTML_TAG_2 = "</html>";
    private static final String HEAD_TAG_1 = "<head>";
    private static final String HEAD_TAG_2 = "</head>";
    private static final String BODY_TAG_1 = "<body>";
    private static final String BODY_TAG_2 = "</body>";
    private static final String DIV_TAG_FINAL = "</div>";
    private static final String FORM_TAG_FINAL = "</form>";
    private DataSource dataSource;

    public Authenticator() {
        PoolProperties tmp = setProprietiesDBC();
        dataSource = new DataSource();
        dataSource.setPoolProperties(tmp);
        System.out.println("Configuration and connection established." + "\n" +
                "You can proceed.");
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletOutputStream out = response.getOutputStream();
        /* Begin HTML Document */
        out.println(HTML_TAG_1);

        /* Begin HEAD */
        out.println(HEAD_TAG_1);

        out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
        out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>");
        out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
        out.println("<meta charset=\"utf-8\">");
        out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
        out.println("<link href=\"https://fonts.googleapis.com/css?family=Muli\" rel=\"stylesheet\">");
        /* End HEAD */
        out.println(HEAD_TAG_2);

        /* Begin BODY */
        out.println(BODY_TAG_1);
        out.println("<div class=\"container\" id=\"firstBucket\" style=\"margin-top:25px; font-family: 'Muli', sans-serif;\">");
        out.println("<div class=\"jumbotron\" id=\"firstJumbo\">");
        out.println("<form action=\"/check.jsp\" method=\"POST\">");
        out.println("<div class=\"form-group\">");
        out.println("<label for=\"username\">Username:</label>");
        out.println("<input type=\"text\" id=\"username\" name=\"username\">");
        out.println("<div class=\"form-group\">");
        out.println("<label for=\"pwd\">Password:</label>");
        out.println("<input type=\"password\"  id=\"pwd\" name=\"pwd\">");
        out.println(DIV_TAG_FINAL);
        out.println("<button type=\"submit\" class=\"btn btn-default\">Submit</button>");
        out.println("<input type=\"hidden\" value=redirect_url>");
        out.println(DIV_TAG_FINAL);
        out.println(FORM_TAG_FINAL);
        out.println(DIV_TAG_FINAL);
        out.println(DIV_TAG_FINAL);

        /* End BODY */
        out.println(BODY_TAG_2);

        /* End HTML Document */
        out.println(HTML_TAG_2);
    }

    protected PoolProperties setProprietiesDBC() {
        PoolProperties p = new PoolProperties();
        p.setUrl("jdbc:mysql://localhost:3306/sal_database?&useSSL=false");
        p.setDriverClassName("com.mysql.jdbc.Driver");
        System.out.println("Trying to login to the database...");
        p.setUsername("root");
        p.setPassword("root");
        System.out.println("Done.");
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
                        "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        System.out.println("Configuration almost made.");
        return p;
    }

    public void createAccount(String name, String pwd, String pwd2) {
        Connection con = null;
        try {
            CrypService cs = new CrypService();
            String pwd_tmp = cs.doEncryption(pwd);
            int hash = pwd_tmp.hashCode();
            con = dataSource.getConnection();
            String query = " insert into users(username, password, isLogged, isLocked, isAdmin) " + " values(?, ?, ?, ?, ?)";

            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, name.toString());
            preparedStmt.setString(2, Integer.toString(hash));
            preparedStmt.setBoolean(3, false);
            preparedStmt.setBoolean(4, false);
            preparedStmt.setBoolean(5, true);
            preparedStmt.execute();

            con.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Something went wrong.");
        } finally {
            if (con != null) try {
                con.close();
            } catch (Exception ignore) {
            }
        }
    }


    public void deleteAccout(String name) throws UndefinedAccount {
        Connection con = null;
        Account tmpAcc;
        tmpAcc = getAccount(name);

        if (tmpAcc.equals(null)) {
            throw new UndefinedAccount();
        }

        try {
            System.out.println("TmpAcc: " + tmpAcc);
            con = dataSource.getConnection();
            String auxNick = tmpAcc.getUsername();
            System.out.println("Aux Nick: " + auxNick);
            String query = "UPDATE users SET isLocked=? WHERE username = ?";
            System.out.println("Query: " + query);
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setBoolean(1, true);
            preparedStmt.setString(2, auxNick);
            preparedStmt.execute();
            con.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Something went wrong.");
        } finally {
            if (con != null) try {
                con.close();
            } catch (Exception ignore) {
            }
        }
    }


    public Account getAccount(String username) {
        Connection con = null;
        Account tmpAcc = null;
        try {
            con = dataSource.getConnection();
            String query = "select * from users where username = ?";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String username_to_obj = rs.getString("username");
                String pwdCrypt = rs.getString("password");
                boolean isLogged = rs.getBoolean("isLogged");
                boolean isLocked = rs.getBoolean("isLocked");
                boolean isAdmin = rs.getBoolean("isAdmin");
                tmpAcc = new Account(username_to_obj, pwdCrypt, isLogged, isLocked, isAdmin);
            }
            rs.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            if (con != null) try {
                con.close();
            } catch (Exception ignore) {
            }
        }
        return tmpAcc;
    }


    public void changePassword(String name, String pwd, String pwdNew) throws UndefinedAccount {
        Connection con = null;
        Account tmpAcc;
        tmpAcc = getAccount(name);

        if (tmpAcc.equals(null)) {
            throw new UndefinedAccount();
        }

        try {
            con = dataSource.getConnection();
            String auxNick = tmpAcc.getUsername();
            String query = "UPDATE users SET password = ? WHERE username = ?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, Integer.toString(pwd.hashCode()));
            preparedStmt.setString(2, auxNick);
            preparedStmt.execute();
            con.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Something went wrong.");
        } finally {
            if (con != null) try {
                con.close();
            } catch (Exception ignore) {
            }
        }
    }


    public Account login(String name, String pwd) throws UndefinedAccount, LockedAccount, AuthenticationError {
        Account tmp;
        tmp = getAccount(name);

        if (tmp.equals(null)) {
            return null;
        }

        if (tmp.getLockStatus()) {
            return null;
        }

        try {
            if (tmp.getPassword().equals(Integer.toString(pwd.hashCode()))) {
                Connection con = null;
                try {
                    con = dataSource.getConnection();
                    String username = tmp.getUsername();
                    String query = "UPDATE users SET isLogged=? WHERE username = ?";
                    PreparedStatement preparedStmt = con.prepareStatement(query);
                    preparedStmt.setBoolean(1, true);
                    preparedStmt.setString(2, username);
                    preparedStmt.execute();
                    con.close();
                    return tmp;
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                    System.err.println("Something went wrong.");
                } finally {
                    if (con != null) try {
                        con.close();
                    } catch (Exception ignore) {
                    }
                }
            }
        } catch(Exception e) {
            e.getMessage();
        }
        return null;
    }

    public void logout(Account acc) {
        Connection con = null;
        Account tmpAcc;
        try {
            con = dataSource.getConnection();
            String username = acc.getUsername();
            String query = "UPDATE users SET isLogged=? WHERE username = ?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setBoolean(1, false);
            preparedStmt.setString(2, username);
            preparedStmt.execute();
            con.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Something went wrong.");
        } finally {
            if (con!=null) try {con.close();}catch (Exception ignore) {}
        }
    }
}
