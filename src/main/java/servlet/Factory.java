package servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "servlet.Factory")
public class Factory extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Account account;

    private static final String HTML_TAG_1 = "<html>";
    private static final String HTML_TAG_2 = "</html>";
    private static final String HEAD_TAG_1 = "<head>";
    private static final String HEAD_TAG_2 = "</head>";
    private static final String BODY_TAG_1 = "<body>";
    private static final String BODY_TAG_2 = "</body>";
    private static final String DIV_TAG_FINAL = "</div>";
    private static final String FORM_TAG_FINAL = "</form>";
    private static final String BOOTSTRAP_LINK = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">";
    private static final String JQUERY_LINK = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>";
    private static final String JAVASCRIPT_LINK = "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>";
    private static final String CHARSET = "<meta charset=\"utf-8\">";
    private static final String MOBILE_FRIENDLY = "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
    private static final String FONT_LINK = "<link href=\"https://fonts.googleapis.com/css?family=Muli\" rel=\"stylesheet\">";


    public Factory() {

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletOutputStream out = response.getOutputStream();
        /* Begin HTML Document */
        out.println(HTML_TAG_1);

        /* Begin HEAD */
        out.println(HEAD_TAG_1);
        out.println("<title>Create an account</title>");
        out.println("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");
        out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>");
        out.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>");
        out.println("<meta charset=\"utf-8\">");
        out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
        out.println("<link href=\"https://fonts.googleapis.com/css?family=Muli\" rel=\"stylesheet\">");
        /* End HEAD */
        out.println(HEAD_TAG_2);

        /* Begin BODY */
        out.println(BODY_TAG_1);
        out.println("<div class=\"container\" id=\"firstBucket\" style=\"margin-top:25px; font-family: 'Muli', sans-serif;\">");
        out.println("<div class=\"jumbotron\" id=\"firstJumbo\">");
        out.println("<form action=\"/account_made.jsp\" method=\"POST\">");
        out.println("<div class=\"form-group\">");
        out.println("<label for=\"username\">Username:</label>");
        out.println("<input type=\"text\" id=\"username\" name=\"username\">");
        out.println("<div class=\"form-group\">");
        out.println("<label for=\"pwd\">Password:</label>");
        out.println("<input type=\"password\"  id=\"pwd\" name=\"pwd\">");
        out.println(DIV_TAG_FINAL);
        out.println("<label for=\"pwd\">Enter the password again:</label>");
        out.println("<input type=\"password\"  id=\"pwd2\" name=\"pwd2\">");
        out.println(DIV_TAG_FINAL);
        out.println("<button type=\"submit\" class=\"btn btn-default\">Submit</button>");
        out.println("<input type=\"hidden\" value=redirect_url>");
        out.println(DIV_TAG_FINAL);
        out.println(FORM_TAG_FINAL);
        out.println(DIV_TAG_FINAL);
        out.println(DIV_TAG_FINAL);

        /* End BODY */
        out.println(BODY_TAG_2);

        /* End HTML Document */
        out.println(HTML_TAG_2);
    }
}
