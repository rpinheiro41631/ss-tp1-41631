package servlet;

import javax.crypto.*;
import java.security.*;
import java.util.Base64.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class CrypService {
    private static final String ALGORITHM_TO_CRYPTO = "AES";
    private static final byte[] keyValue =
            new byte[] {'F', 'C', 'T', '/', 'U', 'N', 'L', 'r', 'o', 'c', 'k', 's', '!', '!', 'd', 'i'};
    private Key key;

    public CrypService() {
        key = new SecretKeySpec(keyValue, ALGORITHM_TO_CRYPTO);
    }

    public String doEncryption(String decryptedTxt) {
        byte[] encValue = new byte[55536];
        try {
            Cipher c = Cipher.getInstance(ALGORITHM_TO_CRYPTO);
            Key tmp = getKey();
            c.init(Cipher.ENCRYPT_MODE, key);
            encValue = c.doFinal(decryptedTxt.getBytes());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return java.util.Base64.getEncoder().encodeToString(encValue);
    }

    public String doDecryption(String encryptedTxt) {
        byte[] decValue = new byte[55536];
        try {
            Cipher c = Cipher.getInstance(ALGORITHM_TO_CRYPTO);
            Key tmp = getKey();
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decodedValue = java.util.Base64.getDecoder().decode(encryptedTxt);
            decValue = c.doFinal(decodedValue);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return new String(decValue);
    }

    public Key getKey() {
        return key;
    }
}
