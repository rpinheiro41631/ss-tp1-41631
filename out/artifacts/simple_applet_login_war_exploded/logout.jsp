<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.Account" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 4:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logout</title>
</head>
<body>
<%
    Authenticator as = new Authenticator();
    Account acc;
    HttpSession hs = request.getSession(true);
    String aux = hs.getAttribute("USER").toString();
    acc = as.getAccount(aux);
    as.logout(acc);
    request.getSession(false);
    if(hs != null) session.invalidate();
    response.sendRedirect("/index.jsp");
%>
</body>
</html>
